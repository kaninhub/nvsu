const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const http = require('http');

const mockdata = require('../mockdata.js')

const app = express();
app.use(bodyParser.json());

require('./middleware/authorization.js')(app);

require('./order/controller')(app);
require('./product/controller')(app);
require('./user/controller')(app);

// check 
app.get('/hello', function (req, res) {
	res.initHeader();
	res.send("It's me. ver: " + VERSION + "\n");
});

try {
	mongoose.Promise = Promise;
	mongoose.connect("mongodb://localhost:27017/test")
		.then(async function () {
            await mockdata.mock()
        })
} catch (err) {
	console.error("Cannot connect to MongoDB: " + err);
	process.exit(1);
}

const server = http.createServer(app);
server.listen(8080);