const Order = require('./model.js')
const Product = require('../product/model.js')

module.exports = function (app) {
    app.post('/order', async function(req, res, next) {
        try {
            const products = await Product.find({}).lean()
            const productMap = {}
            products.forEach((product) => {
                productMap[product._id] = product
            })
            const productList = req.body.productList
            let totalCost = 0
    
            productList.forEach(product => {
                totalCost += productMap[product._id].price * product.number
            });
            
            const buyer = req.headers.username
            const order = await new Order({
                productList,
                totalCost,
                buyer,
            }).save()
            res.status(200).send(order)
        } catch(err) {
            res.status(400).send(err)
        }
        
    })

    app.delete('/order/:id', async function(req, res, next) {
        try {
            const orderId = req.params.id
            const order = await Order.updateOne({ _id: orderId }, { $set: { cancelled: true } })
            res.status(200).send(order)
        } catch(err) {
            res.status(400).send(err)
        }
    })

    app.get('/order/:id', async function(req, res, next) {
        try {
            const orderId = req.params.id
            const order = await Order.findById(orderId)
            res.status(200).send(order)
        } catch(err) {
            res.status(400).send(err)
        }
    })

    app.get('/history/order', async function(req, res, next) {
        try {
            const owner = req.headers.username
            const orders = await Order.find({ buyer: owner })
            res.status(200).send(orders)
        } catch(err) {
            res.status(400).send(err)
        }
    })
};