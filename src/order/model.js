const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    productList: {},
    cancelled: { type: Boolean, default: false },
    totalCost: Number,
    buyer: String,
});
const orderModel = mongoose.model('order', orderSchema);

module.exports = orderModel