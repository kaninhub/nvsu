const Product = require('./model.js');

module.exports = function (app) {
    app.get('/products', async function(req, res, next) {
        const products = await Product.find({}).lean()
        res.status(200).send(products)
    })
    app.get('/product/:productId', async function(req, res, next) {
        const product = await Product.findById(req.params.productId).lean()
        res.status(200).send(product)
    })
};