const User = require('./model.js')

module.exports = function (app) {

    app.get('/user', async function(req, res, next) {
        const username = req.headers.username
        try {
            const user = await User.findOne({ username: username}).lean()
            res.status(200).send(user)
        } catch (err) {
            res.status(400).send('username used already')
        }
    })

    app.post('/user', async function(req, res, next) {
        const username = req.body.username
        const password = req.body.password
        try {
            if(typeof username !== 'string' || typeof password !== 'string' ) {
                res.status(400).send('username and password must be string')
            }
            await new User({ username: username, password: password }).save()
            res.status(201).send('user created')
        } catch (err) {
            res.status(400).send('username used already')
        }
    })

};