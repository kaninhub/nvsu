const User = require('../user/model.js')

module.exports = function (app) {
    app.use(async function(req, res, next) {
        const username = req.headers.username
        const password = req.headers.password
        try {
            console.log(req.path)
            console.log(req.method)
            if (req.path === '/user' && req.method === 'POST') {
                // not authen cause register or login
                next()
            } else {
                const result = await User.findOne({ username: username, password: password })
                if (result) {
                    console.log('correct login!')
                    next()
                } else {
                    throw new Error('incorrect login!')
                }
            }
        } catch(err) {
            res.status(401).send('incorrect login!')
        }
    })
};