const User = require('./src/user/model')
const Product = require('./src/product/model')
const Order = require('./src/order/model')

module.exports = {
    mock: async function() {
        try {
            await new User({ username: 'user1', password: '1234' }).save()
            await new User({ username: 'user2', password: '5678' }).save()
            await new Product({ name: 'product1', number: 100, price: 5 }).save()
            await new Product({ name: 'product2', number: 50, price: 2 }).save()
        } catch(err) {
            console.log(err)
        }
    }
}